# 16 Mhz crystal oscillator enabled
# BODEN:=0 CKOPT:=0 CKSEL:=1111 SUT:=01

# High-Byte: 0xC9
# Low-Byte:  0x9F

avrdude -pm8 -c stk500 -P /dev/ttyUSB0 -U lfuse:w:0x9F:m -U hfuse:w:0xC9:m

