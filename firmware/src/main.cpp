/*  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Modular Synth Drums firmware.
 *
 *  Modular Synth Drums firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Modular Synth Drums firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Modular Synth Drums firmware.  If not, 
 *   see <http://www.gnu.org/licenses/>.
 */
#include <array>
#include <algorithm>

#include "ecpp/Sound/Midi/Definitions.hpp"
#include "ecpp/Sound/Midi/GeneralMidi.hpp"
#include "ecpp/Sound/Midi/Receiver.hpp"

#include "Drums.hpp"

using namespace ecpp::HAL;

namespace Midi        = ecpp::Sound::Midi;
namespace GeneralMidi = ecpp::Sound::Midi::GeneralMidi;

using namespace Drums;




static constexpr uint16_t kUBRR_Value = (F_CPU)/16/Midi::kBaudRate;

static Midi::Receiver<Midi::SingleBuffer> midi_receiver;

void setup() {
  // put your setup code here, to run once:
  PinLed = false;
  PinGateCowBell = false;
  PinGateAccent = false;
  PinGateBassDrum = false;
  PinGateBassDrumAttack = false;
  PinGateClap = false;
  PinGateSnare = false;
  PinGateSnareNoise = false;
  PinGateMaracas = false;
  
  PinLed.enableOutput();
  PinWhiteNoise.enableOutput();
  PinGateCowBell.enableOutput();
  PinGateAccent.disableOutput();
  PinGateBassDrum.enableOutput();
  PinGateBassDrumAttack.disableOutput();
  PinGateClap.enableOutput();
  PinGateSnare.enableOutput();
  PinGateSnareNoise.enableOutput();
  PinGateMaracas.enableOutput();

  PinTriggerBassDrum.disableOutput();
  PinTriggerClap.disableOutput();
  PinTriggerSnare.disableOutput();
  PinTriggerNoise.disableOutput();
  PinTriggerCowBell.disableOutput();
  PinTriggerMaracas.disableOutput();

  sound_.Init();
  
  /* configure UART for MIDI reception */
  UBRRH = (kUBRR_Value >> 8) & 0x7F;
  UBRRL = (kUBRR_Value >> 0) & 0xFF;

  UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);
  UCSRB = _BV(RXEN);

  sei();
}

static uint8_t velocity_accent_threshold = 64;

void process_note_on_msg()
{
  auto reader = midi_receiver.buffer().reader();

  const int8_t note     = reader.data(0);
  const int8_t velocity = reader.data(1);

  if(velocity < 1)
    return;

  /* accent is gobally set on last notes velocity */
  if (velocity > velocity_accent_threshold)
    PinGateAccent.disableOutput();
  else
    PinGateAccent.enableOutput();

  switch(note)
  {
  case GeneralMidi::Percussion::Note::kElectricBassDrum:
    sound_.kick.DoTrigger(); 
    break;
  case GeneralMidi::Percussion::Note::kHandClap:         
    sound_.clap.DoTrigger(); 
    break;
  case GeneralMidi::Percussion::Note::kElectricSnare:
    if(sound_.snare_tom.CanTrigger() && sound_.snare_noise.CanTrigger())
    {
      sound_.snare_tom.DoTrigger(); 
      sound_.snare_noise.DoTrigger();
    } 
    break;
  case GeneralMidi::Percussion::Note::kClosedHiHat:
    sound_.snare_noise.DoTrigger(); 
    break;
  case GeneralMidi::Percussion::Note::kHighTom:
    sound_.snare_tom.DoTrigger();
    break;
  case GeneralMidi::Percussion::Note::kCowBell:
    sound_.TriggerTR808Cowbell(); 
    break;
  case GeneralMidi::Percussion::Note::kMaracas:
    sound_.maracas.DoTrigger(); 
    break;
  default: break;
  }
}


void process_cowbell_note_on_msg()
{
  auto reader = midi_receiver.buffer().reader();

  const int8_t note     = reader.data(0);
  const int8_t velocity = reader.data(1);

  if(velocity < 1)
    return;

  /* accent is gobally set on last notes velocity */
  if (velocity > velocity_accent_threshold)
    PinGateAccent.disableOutput();
  else
    PinGateAccent.enableOutput();

  sound_.TriggerCowbellNote(note);
}


void process_control_change()
{
  auto reader = midi_receiver.buffer().reader();

  const int8_t controller = reader.data(0);
  const int8_t value      = reader.data(1);

  switch(controller)
  {
  case Midi::kControllerExpression: velocity_accent_threshold = value;
  default: break;
  }
}

void loop() {

  sound_.kick.CheckTrigger(PinTriggerBassDrum);
  sound_.snare_noise.CheckTrigger(PinTriggerNoise);
  sound_.snare_tom.CheckMultiTrigger(PinTriggerSnare, sound_.snare_noise);
  sound_.clap.CheckTrigger(PinTriggerClap);
  sound_.maracas.CheckTrigger(PinTriggerMaracas);
  sound_.cowbell.CheckTrigger(PinTriggerCowBell);


  if(UCSRA & _BV(RXC))
  {
    uint8_t rx_data = UDR;

    if(midi_receiver.ProcessCharacter(rx_data))
    { /* new midi message received */
      auto reader = midi_receiver.buffer().reader();

      switch(reader.status())
      {
      case (Midi::kStatusNoteOn | GeneralMidi::Percussion::kChannel):  
        process_note_on_msg(); 
        break;
      case (Midi::kStatusNoteOn | (GeneralMidi::Percussion::kChannel + 1)):  
        process_cowbell_note_on_msg();
        break;

      case (Midi::kStatusControlChange | GeneralMidi::Percussion::kChannel):  
        process_control_change(); 
        break;
      default: 
        break;
      }

      reader.next();
      PinLed = true;
    }
  }


  PinLed = sound_.kick.IsActive() or
           sound_.snare_tom.IsActive() or
           sound_.snare_noise.IsActive() or
           sound_.clap.IsActive() or
           sound_.maracas.IsActive() or
           sound_.cowbell.IsActive(); 
}

int main()
{
  setup();

  while(true)
    loop();
}