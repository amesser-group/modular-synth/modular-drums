/*  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Modular Synth Drums firmware.
 *
 *  Modular Synth Drums firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Modular Synth Drums firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Modular Synth Drums firmware.  If not, 
 *   see <http://www.gnu.org/licenses/>.
 *  */
#ifndef MODULAR_DRUMS_HPP_
#define MODULAR_DRUMS_HPP_

#include <tuple>

#include "ecpp/HAL/AVR8.hpp"


namespace Drums 
{
  using namespace ecpp::HAL;

  ISR(TIMER1_COMPA_vect);
  ISR(TIMER1_COMPB_vect);
  ISR(TIMER2_COMP_vect);

  static constexpr IOPin<AVR_IO_PB0> PinLed;
  static constexpr IOPin<AVR_IO_PB1> PinToneCowBellHigh;
  static constexpr IOPin<AVR_IO_PB2> PinToneCowBellLow;
  static constexpr IOPin<AVR_IO_PB3> PinWhiteNoise;
  static constexpr IOPin<AVR_IO_PB4> PinGateAccent;

  static constexpr IOPin<AVR_IO_PC0> PinTriggerNoise;
  static constexpr IOPin<AVR_IO_PC1> PinTriggerMaracas;
  static constexpr IOPin<AVR_IO_PC2> PinTriggerClap;
  static constexpr IOPin<AVR_IO_PC3> PinTriggerSnare;
  static constexpr IOPin<AVR_IO_PC4> PinTriggerBassDrum;
  static constexpr IOPin<AVR_IO_PC5> PinTriggerCowBell;

  static constexpr IOPin<AVR_IO_PD1> PinGateCowBell;
  static constexpr IOPin<AVR_IO_PD2> PinGateBassDrum;
  static constexpr IOPin<AVR_IO_PD3> PinGateBassDrumAttack;
  static constexpr IOPin<AVR_IO_PD4> PinGateSnareNoise;
  static constexpr IOPin<AVR_IO_PD5> PinGateSnare;
  static constexpr IOPin<AVR_IO_PD6> PinGateClap;
  static constexpr IOPin<AVR_IO_PD7> PinGateMaracas;

  /* Defines the gate sequences for each sound generator
  * 
  * Stepped with 2.5kHz -> 0.4ms Each step */
  typedef std::array<uint8_t, 128> GateSequence;

  /** Contain to hold state & sequencing information for one trigger */
  class TriggerHandler
  {
  public:
  
    void CheckTrigger(uint8_t mask);

    bool IsActive()   const { return (state_ + 1U) < std::tuple_size<GateSequence>::value; }
    bool CanTrigger() const { return state_ >= 25U; }
    void DoTrigger() { if(CanTrigger()) state_ = 0; }

    template<int PIN>
    void CheckTrigger(const IOPin<PIN> &pin) {CheckTrigger(pin.MASK);}

    template<int PIN>
    void CheckMultiTrigger(const IOPin<PIN> &pin, TriggerHandler & other) 
    {
      if(other.CanTrigger())
      {
        CheckTrigger(pin.MASK);
        other.CheckTrigger(pin.MASK);      
      }
    }

    uint_least8_t & state() { return state_; }
    
  protected:
    uint_least8_t last_active_ = 0;
    uint_least8_t state_       = 0;
  };


  struct Sound
  {
  public:
    static void Init();

    void TriggerCowbellNote(int8_t note);
    void TriggerTR808Cowbell();

    void SetCowbellNote(int8_t note, uint8_t low);

    TriggerHandler kick;
    TriggerHandler snare_tom;
    TriggerHandler snare_noise;
    TriggerHandler clap;
    TriggerHandler maracas;
    TriggerHandler cowbell;    

    uint_least16_t  cowbell_divide_[2];
    uint_least8_t   cowbell_cnt_[2];
    uint_least8_t   cowbell_oct_[2];
  };

  /* one global instance of triggers */
  extern Sound   sound_;
  
}
#endif