/*  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Modular Synth Drums firmware.
 *
 *  Modular Synth Drums firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Modular Synth Drums firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Modular Synth Drums firmware.  If not, 
 *   see <http://www.gnu.org/licenses/>.
 */
#include <array>

#include "ecpp/Sound/Midi/Definitions.hpp"
#include "Drums.hpp"

using namespace Drums;
using namespace ecpp::Sound::Midi;

Sound Drums::sound_;

static constexpr uint8_t make_kick_sequence(uint8_t state)
{
  switch(state)
  {
  default:          return PinGateBassDrum.MASK ;
  case 1          : return PinGateBassDrum.MASK | PinGateBassDrumAttack.MASK;
  case 2  ... 4   : return                        PinGateBassDrumAttack.MASK;
  case 5  ... 14  : return PinGateBassDrum.MASK | PinGateBassDrumAttack.MASK;
  }
}

static constexpr uint8_t make_snare_tom_sequence(uint8_t state)
{
  switch(state)
  {
  default:        return PinGateSnare.MASK;
  case 1:         return PinGateSnare.MASK;
  case 2 ... 6:   return 0;
  }
}

static constexpr uint8_t make_snare_noise_sequence(uint8_t state)
{
  switch(state)
  {
  default:        return PinGateSnareNoise.MASK;
  case 1:         return PinGateSnareNoise.MASK;
  case 2 ... 6:   return 0;
  }
}

static constexpr uint8_t make_clap_sequence(uint8_t state)
{
  switch(state)
  {
  default:         return PinGateClap.MASK;
  case 1:          return PinGateClap.MASK;
  case 2  ... 6:   return 0;
  case 7  ... 28:  return PinGateClap.MASK;
  case 29 ... 33:  return 0;
  case 34 ... 55:  return PinGateClap.MASK;
  case 56 ... 60:  return 0;
  case 61 ... 75:  return PinGateClap.MASK;
  case 76 ... 80:  return 0;
  }
}

static constexpr uint8_t make_maracas_sequence(uint8_t state)
{
  switch(state)
  {
  default:         return PinGateMaracas.MASK;
  case 1:          return PinGateMaracas.MASK;
  case 2  ...  51: return 0;
  }
}

static constexpr uint8_t make_cowbell_sequence(uint8_t state)
{
  switch(state)
  {
  default:         return PinGateCowBell.MASK;
  case 1:          return PinGateCowBell.MASK;
  case 2  ...  22: return 0;
  }
}

static constexpr uint8_t make_sequence(uint8_t state)
{
  return make_kick_sequence(state) bitor
         make_snare_tom_sequence(state) bitor
         make_snare_noise_sequence(state) bitor
         make_clap_sequence(state) bitor
         make_maracas_sequence(state) bitor
         make_cowbell_sequence(state);
}

const GateSequence gate_sequence PROGMEM {[]<unsigned int...I>(std::index_sequence<I...>)
  {
    return GateSequence{make_sequence(I)...};
  }(std::make_index_sequence<gate_sequence.size()>{})
};

static void UpdateGate(uint8_t & state, uint8_t mask)
{
  auto p    = IOPort<AVR_IO_PD>();
  uint8_t s = (state + 1) % gate_sequence.size();

  state = std::max(state, s);

  uint8_t seq = pgm_read_byte(&gate_sequence[s]);

  p.updateDirection(seq, mask);
}

static void handle_kick()
{
  UpdateGate(sound_.kick.state(), PinGateBassDrum.MASK | PinGateBassDrumAttack.MASK);
}

static void handle_snare_tom()
{
  UpdateGate(sound_.snare_tom.state(), PinGateSnare.MASK);
}

static void handle_snare_noise()
{
  UpdateGate(sound_.snare_noise.state(), PinGateSnareNoise.MASK);
}

static void handle_clap()
{
  UpdateGate(sound_.clap.state(), PinGateClap.MASK);
}

static void handle_maracas()
{
  UpdateGate(sound_.maracas.state(), PinGateMaracas.MASK);
}

static void handle_cowbell()
{
  UpdateGate(sound_.cowbell.state(), PinGateCowBell.MASK);
}

static uint16_t lfsr = 0xACE1;

static void
MakeNoise()
{
  /* taps: 16 14 13 11; characteristic polynomial: x^16 + x^14 + x^13 + x^11 + 1 */
  lfsr = (lfsr >> 1) ^ (-(lfsr & 1u) & 0xB400u);    

  PinWhiteNoise = (lfsr & 0x1) != 0;
}

static constexpr unsigned int isr_freq = 20000;

ISR(TIMER1_COMPA_vect)
{
  OCR1A += sound_.cowbell_divide_[0];
  sound_.cowbell_cnt_[0]++;

  if(sound_.cowbell_cnt_[0] & sound_.cowbell_oct_[0])
    PinToneCowBellHigh = true;
  else
    PinToneCowBellHigh = false;
}

ISR(TIMER1_COMPB_vect)
{
  OCR1B += sound_.cowbell_divide_[1];
  sound_.cowbell_cnt_[1]++;

  if(sound_.cowbell_cnt_[1] & sound_.cowbell_oct_[1])
    PinToneCowBellLow = true;
  else
    PinToneCowBellLow = false;
}

static uint_least8_t drum_divide   = 0;

static void handle_drums()
{
  auto drum = (drum_divide++) % 8;

  switch(drum)
  {
   case 0: handle_kick(); break;
   case 1: handle_snare_tom(); break;
   case 2: handle_snare_noise(); break;
   case 3: handle_clap(); break;
   case 4: handle_maracas(); break;
   case 5: handle_cowbell(); break;
  }
}

/** Interrupt service routine called with 20kHz frequency */
ISR(TIMER2_COMP_vect)
{
  MakeNoise();

  handle_drums();
}

void 
Sound::Init()
{
  PinToneCowBellHigh.enableOutput();
  PinToneCowBellLow.enableOutput();

  sound_.TriggerTR808Cowbell();

  /* Generate 20 kHz Interrupt from 16 MHz main clock */
  OCR2  = F_CPU / 32 / 20000 - 1;
  TCCR2 = _BV(WGM21) | (0x3 << CS20);

  /* Configure Timer 1 for ctc mode / icr1, 2 Mhz base clock */
  ICR1   = 0xFFFF;
  TCCR1A = 0;
  TCCR1B = _BV(WGM13) | _BV(WGM12) |  (2 << CS10);

  /* Enable interrupts */
  TIMSK  = _BV(OCIE1A) | _BV(OCIE1B) | _BV(OCIE2);
}


using OctaveDividers = std::array<uint16_t, 12>;

static constexpr OctaveDividers kOctaveDividers PROGMEM =
{
  478, 451, 426, 402, 379, 358, 338, 319, 301, 284, 268, 253
};


void 
Sound::SetCowbellNote(int8_t note, uint8_t low)
{
  uint8_t subdivider = 0x40;
    
  /* move to correct octave */
  note = note - Note::kC1;
  while(note >= 12)
  {
    note -= 12;
    subdivider >>= 1;
  }

  cowbell_divide_[low] = pgm_read_word(&kOctaveDividers[note]);
  cowbell_oct_[low]    = subdivider;
}


void
Sound::TriggerCowbellNote(int8_t note)
{
  if(not cowbell.CanTrigger())
    return;

  if(note > Note::kB7)
    return;

  if(note < Note::kGb1)
    return;

  SetCowbellNote(note, 0);
  SetCowbellNote(note - 6, 1);

  cowbell.DoTrigger();
}

void 
Sound::TriggerTR808Cowbell()
{
  if(not cowbell.CanTrigger())
    return;

  /* 800 Hz */
  cowbell_divide_[0] = 1250;
  cowbell_oct_[0]    = 0x01;

  /* 540 Hz */
  cowbell_divide_[1] = 1852;
  cowbell_oct_[1]    = 0x01;

  cowbell.DoTrigger();
}

void 
TriggerHandler::CheckTrigger(uint8_t mask)
{
  if(CanTrigger())
  {
    IOPort<AVR_IO_PC0> port;

    auto active = port.getInputs() & mask;  

    if(active && (last_active_ == false))
      state_ = 0;

    last_active_ = active;
  }
}