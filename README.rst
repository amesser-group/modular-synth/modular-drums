###################################
Eurorack Modular Synth Drums Module
###################################

.. image:: doc/media/front_without_knobs.jpg

Overview
========

Purpose of this project is to create a simple drum module for
use with `Döpfer A-100`_ Eurorack system.

.. _Döpfer A-100: https://doepfer.de/home_d.htm

Status
======

Schematic & PCB is ready a first version has been built. Turned out, that the
Schematic had one error which needs some trace cutting to be fixed. During 
building some component values have been adjusted to more standard ones.

.. image:: doc/media/pcb_component.jpg

A basic software for the module is ready to support sound triggering via 
Trigger-Inputs and via TRS Midi Input

Erata
=====

Revision 1
----------

- Wrong connection of R325 and C315 to GND. Should be connected to -12V
- Maracas sound a bit harsch


Licensing
=========

All files of the project are licensed under the conditions of 
CC BY-SA license. Software files are covered by GPL v3 if
not otherwise noted in the files


Copyright
=========

If not otherwise noted within the files, the copyright for 
all files in this project folder is
(c) 2022 Andreas Messer <andi@bastelmap.de>.

Links
=====

.. target-notes::


