EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "Modular Drums"
Date "2023-01-03"
Rev "2"
Comp "(c) 2022-2023 Andreas Messer"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1000 1000 1000 1500
U 635986C0
F0 "Control Board" 50
F1 "control.sch" 50
$EndSheet
$Sheet
S 2250 1000 800  1500
U 635B4852
F0 "Sound Board" 50
F1 "sound.sch" 50
$EndSheet
$Comp
L Mechanical:Housing N101
U 1 1 63B7A8AF
P 1250 3000
F 0 "N101" H 1403 3034 50  0000 L CNN
F 1 "Housing" H 1403 2943 50  0000 L CNN
F 2 "EurorackModular:A100_16TE" H 1300 3050 50  0001 C CNN
F 3 "~" H 1300 3050 50  0001 C CNN
	1    1250 3000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
